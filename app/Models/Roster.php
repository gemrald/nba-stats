<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roster extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roster';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','team_code','number','name','pos','height','weight','dob','nationality','years_exp','college'];

    /**
     * Get the stat record associated with the roster/player.
     */
    public function stat()
    {
        return $this->hasOne(Stat::class,'player_id','id');
    }

    /**
     * Get the team that owns the roster.
     */
    public function team()
    {
        return $this->belongsTo(Team::class,'team_code','code');
    }
}
