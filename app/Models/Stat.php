<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_totals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['player_id',
                            'age',
                            'games',
                            'games_started',
                            'minutes_played',
                            'field_goals',
                            'field_goals_attempted',
                            'free_throws',
                            'free_throws_attempted',
                            'offensive_rebounds',
                            'defensive_rebounds',
                            'assists',
                            'steals',
                            'blocks',
                            'turnovers',
                            'personal_fouls'];

    // Column aliases
    protected $maps = [
                        '3pt' => 'three_pt',
                        '3pt_attempted' => 'three_pt_attempted',
                        '2pt' => 'two_pt',
                        '2pt_attempted' => 'two_pt_attempted'
                        ];

    protected $append = [
                        'three_pt', 
                        'three_pt_attempted',
                        'three_pt_pct',
                        'two_pt',
                        'two_pt_attempted',
                        'two_pt_pct',
                        'field_goals_pct',
                        'free_throws_pct',
                        'total_rebounds',
                        'total_points'
                        ];

    protected $hidden = [
                        '3pt', 
                        '3pt_attempted',
                        '2pt',
                        '2pt_attempted'
                        ];

    /**
     * Get the roster that owns the stats.
     */
    public function roster()
    {
        return $this->belongsTo(Roster::class,'player_id','id');
    }

    public function getThreePtAttribute()
    {
        return $this->attributes['3pt'];
    }

    public function getThreePtAttemptedAttribute()
    {
        return $this->attributes['3pt_attempted'];
    }

    public function getThreePtPctAttribute()
    {
        return $this->attributes['three_pt_pct'] = $this->three_pt ? round($this->three_pt / $this->three_pt_attempted, 2) * 100 . ' %' : 0  ;
    }

    public function getTwoPtAttribute()
    {
        return $this->attributes['2pt'];
    }

    public function getTwoPtAttemptedAttribute()
    {
        return $this->attributes['2pt_attempted'];
    }

    public function getTwoPtPctAttribute()
    {
        return $this->attributes['two_pt_pct'] = $this->two_pt ? round($this->two_pt / $this->two_pt_attempted, 2) * 100 . ' %' : 0  ;
    }

    public function getFieldGoalsPctAttribute()
    {
        return $this->attributes['field_goals_pct'] = $this->field_goals ? round($this->field_goals / $this->field_goals_attempted, 2) * 100 . ' %' : 0  ;
    }

    public function getFreeThrowsPctAttribute()
    {
        return $this->attributes['free_throws_pct'] = $this->free_throws ? round($this->free_throws / $this->free_throws_attempted, 2) * 100 . ' %' : 0  ;
    }

    public function getTotalReboundsAttribute()
    {
        return $this->attributes['total_rebounds'] = ($this->offensive_rebounds + $this->defensive_rebounds);
    }

    public function getTotalPointsAttribute()
    {
        return $this->attributes['total_points'] = ($this->free_throws + ($this->three_pt * 3) + ($this->two_pt * 2));
    }

}
