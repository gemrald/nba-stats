<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Stat;
use App\Http\Resources\StatResource;

class StatRepository implements StatRepositoryInterface
{

    /**
     * @var Stat
     */
    protected $stat;
    /**
     * StatController constructor.
     *
     * @param Stat $stat
     */
    public function __construct(Stat $stat)
    {
        $this->stat = $stat;
    }

    public function all()
    {
        $stats = $this->stat->all();
        return StatResource::collection($stats);
    }
 
}