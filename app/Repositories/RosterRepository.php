<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Models\Roster;
use App\Http\Resources\RosterResource;

class RosterRepository implements RosterRepositoryInterface
{

    /**
     * @var Roster
     */
    protected $roster;
    /**
     * RosterController constructor.
     *
     * @param Roster $roster
     */
    public function __construct(Roster $roster)
    {
        $this->roster = $roster;
    }

    public function all()
    {
        $rosters = $this->roster->all();
        return RosterResource::collection($rosters);
    }
 
}