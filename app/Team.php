<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'team';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code','name'];

    /**
     * Get the roster record associated with the team.
     */
    public function roster()
    {
        return $this->hasOne(Roster::class,'team_code');
    }

}
