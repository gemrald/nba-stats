<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\RosterRepository;
use App\Models\Roster;

class RosterController extends Controller
{

    // space that we can use the repository from
    protected $model;

    public function __construct(Roster $roster)
    {
       // set the model
       $this->model = new RosterRepository($roster);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of($this->model->all())
            ->addIndexColumn()
            ->make(true);
        }
        return view('rosters');
    }

}