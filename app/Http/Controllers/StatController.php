<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\StatRepository;
use App\Models\Stat;

class StatController extends Controller
{

    // space that we can use the repository from
    protected $model;

    public function __construct(Stat $stat)
    {
       // set the model
       $this->model = new StatRepository($stat);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            return datatables()->of($this->model->all())
            ->addIndexColumn()
            ->make(true);
        }
        return view('stats');
    }

}