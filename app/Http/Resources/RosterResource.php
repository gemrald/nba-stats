<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RosterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'team_code'     => $this->resource->team->code,
            'team_name'     => $this->resource->team->name,
            'id'            => $this->resource->id,
            'name'          => $this->resource->name,
            'number'        => $this->resource->number,
            'pos'           => $this->resource->pos,
            'height'        => $this->resource->height,
            'weight'        => $this->resource->weight,
            'dob'           => $this->resource->dob,
            'nationality'   => $this->resource->nationality,
            'years_exp'     => $this->resource->years_exp,
            'college'       => $this->resource->college,
        ];
    }
}