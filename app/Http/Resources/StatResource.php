<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'team_code'             => $this->resource->roster->team_code,
            'name'                  => $this->resource->roster->name,
            'player_id'             => $this->resource->player_id,
            'age'                   => $this->resource->age,
            'games'                 => $this->resource->games,
            'games_started'         => $this->resource->games_started,
            'minutes_played'        => $this->resource->minutes_played,
            'field_goals'           => $this->resource->field_goals,
            'field_goals_attempted' => $this->resource->field_goals_attempted,
            'field_goals_pct'       => $this->resource->field_goals_pct,
            'three_pt'              => $this->resource->three_pt,
            'three_pt_attempted'    => $this->resource->three_pt_attempted,
            'three_pt_pct'          => $this->resource->three_pt_pct,
            'two_pt'                => $this->resource->two_pt,
            'two_pt_attempted'      => $this->resource->two_pt_attempted,
            'two_pt_pct'            => $this->resource->two_pt_pct,
            'free_throws'           => $this->resource->free_throws,
            'free_throws_attempted' => $this->resource->free_throws_attempted,
            'free_throws_pct'       => $this->resource->free_throws_pct,
            'offensive_rebounds'    => $this->resource->offensive_rebounds,
            'defensive_rebounds'    => $this->resource->defensive_rebounds,
            'total_rebounds'        => $this->resource->total_rebounds,
            'assists'               => $this->resource->assists,
            'steals'                => $this->resource->steals,
            'blocks'                => $this->resource->blocks,
            'turnovers'             => $this->resource->turnovers,
            'personal_fouls'        => $this->resource->personal_fouls,
            'total_points'          => $this->resource->total_points,
        ];
    }
}