@extends('layouts.app')

@section('title', 'NBA 2019 Playoffs')
@section('subtitle', 'Statistics')

@section('content')
   <table class="table table-bordered table-striped" id="stats_datatable">
      <thead>
         <tr>
            <th>ID</th>
            <th>No</th>
            <th>Team</th>
            <th>Name</th>
            <th>Games</th>
            <th>Games Started</th>
            <th>Minutes Played</th>
            <th>Field Goals</th>
            <th>Field Goals Attempted</th>
            <th>Field Goals %</th>
            <th>3pt</th>
            <th>3pt Attempted</th>
            <th>3pt %</th>
            <th>2pt</th>
            <th>2pt Attempted</th>
            <th>2pt %</th>
            <th>Free Throws</th>
            <th>Free Throws Attempted</th>
            <th>Free Throws %</th>
            <th>Offensive Rebounds</th>
            <th>Defensive Rebounds</th>
            <th>Total Rebounds</th>
            <th>Assists</th>
            <th>Steals</th>
            <th>Blocks</th>
            <th>Turnovers</th>
            <th>Personal Fouls</th>
            <th>Total Points</th>
         </tr>
      </thead>
   </table>
@endsection

@section('script')
   <script>
   var SITEURL = '{{URL::to('')}}';
    $(document).ready( function () {
      $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });
     $('#stats_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
             url: SITEURL + "/stats",
             type: 'GET',
            },
            columns: [
                     { data: 'player_id', name: 'player_id', 'visible': false},
                     { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                     { data: 'team_code', name: 'team_code' },
                     { data: 'name', name: 'name' },
                     { data: 'games', name: 'games' },
                     { data: 'games_started', name: 'games_tarted' },
                     { data: 'minutes_played', name: 'minutes_played' },
                     { data: 'field_goals', name: 'field_goals' },
                     { data: 'field_goals_attempted', name: 'field_goals_attempted' },
                     { data: 'field_goals_pct', name: 'field_goals_pct' },
                     { data: 'three_pt', name: 'three_pt' },
                     { data: 'three_pt_attempted', name: 'three_pt_attempted' },
                     { data: 'three_pt_pct', name: 'three_pt_pct' },
                     { data: 'two_pt', name: 'two_pt' },
                     { data: 'two_pt_attempted', name: 'two_pt_attempted' },
                     { data: 'two_pt_pct', name: 'two_pt_pct' },
                     { data: 'free_throws', name: 'free_throws' },
                     { data: 'free_throws_attempted', name: 'free_throws_attempted' },
                     { data: 'free_throws_pct', name: 'free_throws_pct' },
                     { data: 'offensive_rebounds', name: 'offensive_rebounds' },
                     { data: 'defensive_rebounds', name: 'defensive_rebounds' },
                     { data: 'total_rebounds', name: 'total_rebounds' },
                     { data: 'assists', name: 'assists' },
                     { data: 'steals', name: 'steals' },
                     { data: 'blocks', name: 'blocks' },
                     { data: 'turnovers', name: 'turnovers' },
                     { data: 'personal_fouls', name: 'personal_fouls' },
                     { data: 'total_points', name: 'total_points' },
                  ],
            order: [[0, 'desc']],
            dom: 'Bfrtip',
            buttons: [
               'copy', 'csv', 'excel', 'pdf', 'print'
            ]
         });
      });

   </script>
@endsection