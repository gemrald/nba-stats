@extends('layouts.app')

@section('title', 'NBA 2019 Playoffs')
@section('subtitle', 'Players')

@section('content')
   <table class="table table-bordered table-striped" id="players_datatable">
      <thead>
         <tr>
            <th>ID</th>
            <th>No</th>
            <th>Team Code</th>
            <th>Team Name</th>
            <th>Player's Name</th>
            <th>Jersey No.</th>
            <th>Position</th>
            <th>Height</th>
            <th>Weight</th>
            <th>Date of Birth</th>
            <th>Nationality</th>
            <th>Years Exp.</th>
            <th>College</th>
         </tr>
      </thead>
   </table>
@endsection

@section('script')
   <script>
   var SITEURL = '{{URL::to('')}}';
    $(document).ready( function () {
      $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });

     $('#players_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
             url: SITEURL + "/players",
             type: 'GET',
            },
            columns: [
                     { data: 'id', name: 'id', 'visible': true},
                     { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                     { data: 'team_code', name: 'team_code' },
                     { data: 'team_name', name: 'team_name' },
                     { data: 'name', name: 'name' },
                     { data: 'number', name: 'number' },
                     { data: 'pos', name: 'pos' },
                     { data: 'height', name: 'height' },
                     { data: 'weight', name: 'weight' },
                     { data: 'dob', name: 'dob' },
                     { data: 'nationality', name: 'nationality' },
                     { data: 'years_exp', name: 'years_exp' },
                     { data: 'college', name: 'college' },
                  ],
            order: [[0, 'desc']],
            dom: 'Bfrtip',
            buttons: [
               'copy', 'csv', 'excel', 'pdf', 'print'
            ]
         });
      });

   </script>
@endsection