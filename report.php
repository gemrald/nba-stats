<?php

/**
 * Use this file to output reports required for the SQL Query Design test.
 * An example is provided below. You can use the `asTable` method to pass your query result to,
 * to output it as a styled HTML table.
 */

$database = 'nba2019';
require_once('vendor/autoload.php');
require_once('include/utils.php');

/*
 * Example Query
 * -------------
 * Retrieve all team codes & names
 */
echo '<h1>Example Query</h1>';
$teamSql = "SELECT * FROM team";
$teamResult = query($teamSql);
// dd($teamResult);
echo asTable($teamResult);

/*
 * Report 1
 * --------
 * Produce a query that reports on the best 3pt shooters in the database that are older than 30 years old. Only 
 * retrieve data for players who have shot 3-pointers at greater accuracy than 35%.
 * 
 * Retrieve
 *  - Player name
 *  - Full team name
 *  - Age
 *  - Player number
 *  - Position
 *  - 3-pointers made %
 *  - Number of 3-pointers made 
 *
 * Rank the data by the players with the best % accuracy first.
 */
echo '<h1>Report 1 - Best 3pt Shooters</h1>';
// write your query here
$teamSql = "SELECT roster.name AS player_name, team.name AS full_team_name, player_totals.age AS age, roster.number AS player_number, roster.pos AS position, (round(player_totals.3pt / player_totals.3pt_attempted, 2) * 100) AS three_pt_percentage, player_totals.3pt AS three_pt_made FROM team LEFT JOIN roster ON team.code = roster.team_code LEFT JOIN player_totals ON roster.id = player_totals.player_id WHERE player_totals.age > 30 and (round(player_totals.3pt / player_totals.3pt_attempted, 2) * 100) > 35  ORDER BY three_pt_percentage DESC";
$teamResult = query($teamSql);
echo asTable($teamResult);

/*
 * Report 2
 * --------
 * Produce a query that reports on the best 3pt shooting teams. Retrieve all teams in the database and list:
 *  - Team name
 *  - 3-pointer accuracy (as 2 decimal place percentage - e.g. 33.53%) for the team as a whole,
 *  - Total 3-pointers made by the team
 *  - # of contributing players - players that scored at least 1 x 3-pointer
 *  - of attempting player - players that attempted at least 1 x 3-point shot
 *  - total # of 3-point attempts made by players who failed to make a single 3-point shot.
 * 
 * You should be able to retrieve all data in a single query, without subqueries.
 * Put the most accurate 3pt teams first.
 */
echo '<h1>Report 2 - Best 3pt Shooting Teams</h1>';
// write your query here
$teamSql = "SELECT team.name as team_name, (round(sum(player_totals.3pt) / sum(player_totals.3pt_attempted), 2) * 100) AS three_pt_accuracy_percentage , SUM(player_totals.3pt) AS three_pt_made,SUM(player_totals.3pt_attempted) AS total_three_pt_made, SUM(player_totals.3pt_attempted - player_totals.3pt) AS total_three_pt_attempt_failed_made FROM team LEFT JOIN roster ON team.code = roster.team_code LEFT JOIN player_totals ON roster.id = player_totals.player_id GROUP BY team.name ORDER BY three_pt_accuracy_percentage DESC";
$teamResult = query($teamSql);
echo asTable($teamResult);

?>