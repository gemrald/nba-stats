My general comments on the code:
1. Need to fully refactor it according to laravel standards or best practices.
2. Should implement MVC (Model-View-Controller) concept
3. Classes/Ojects should be reusable, easy to maintain and concise 
4. Should follow naming conventions.
5. Should implement any design pattern

My recommendations for the nba2019 DB for improvements:
1. Need to normalize the tables further.
2. Need to add autoincrement id for team tables
3. Need to put the age column in the roster table instead in player_totals table
4. Need to break name column into separate columns e.g. first_name, last_name, middle_initial 
5. Don't use numbers as a starting column name.
6. In extending the DB, update the players_total table to include date/year for new seasons

